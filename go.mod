module gitlab.com/xilix-systems-llc/go-native-ads/v4

go 1.13

require (
	github.com/rs/zerolog v1.20.0
	github.com/zenazn/goji v0.9.0 // indirect
	go.uber.org/atomic v1.7.0
	golang.org/x/lint v0.0.0-20200302205851-738671d3881b // indirect
	golang.org/x/tools v0.0.0-20200324053659-5c746ccfa245 // indirect
)
