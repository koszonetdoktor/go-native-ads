module gitlab.com/xilix-systems-llc/go-native-ads/examples/simple

go 1.15

require (
	github.com/rs/zerolog v1.20.0
	gitlab.com/xilix-systems-llc/go-native-ads/v4 v4.0.6
)

replace gitlab.com/xilix-systems-llc/go-native-ads/v4 v4.0.6 => ../../
